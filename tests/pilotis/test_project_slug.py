from hamcrest import assert_that, is_

from pilotis.commands.project_slug import slugify


def test_slugify_lower_case_names():
    # Given
    project_name = "SoMePrOjEcT"

    # When
    slug = slugify(project_name)

    # Then
    assert_that(slug, is_("someproject"))


def test_slugify_replaces_underscore_by_dash():
    # Given
    project_name = "some_project"

    # When
    slug = slugify(project_name)

    # Then
    assert_that(slug, is_("some-project"))


def test_slugify_replaces_spaces_by_dash():
    # Given
    project_name = "some project"

    # When
    slug = slugify(project_name)

    # Then
    assert_that(slug, is_("some-project"))
