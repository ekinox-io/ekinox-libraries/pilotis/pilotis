import os
from pathlib import Path
from typing import Set
from unittest.mock import call

import pytest
from click.testing import CliRunner
from hamcrest import assert_that, contains_string, equal_to, matches_regexp, not_
from pytest_mock import MockerFixture

from pilotis.cli import main
from pilotis.commands import OPTION_PROJECT_NAME, OPTION_PROJECT_SLUG
from pilotis.commands.aws import (
    OPTION_AWS_DATA_BUCKET_NAME,
    OPTION_AWS_TERRAFORM_BUCKET_DIRECTORY,
    OPTION_AWS_TERRAFORM_BUCKET_NAME,
)
from pilotis.commands.init import OPTION_TOOLS_TO_SETUP, Tools
from pilotis.commands.project_slug import slugify
from tests.pilotis.test_aws import (
    AWS_DATA,
    DATA_BUCKET_NAME,
    PROJECT_PATH_ON_BUCKET,
    TF_BUCKET_NAME,
)
from tests.pilotis.test_git import GITLAB_DATA
from tests.pilotis.test_init import TEST_PROJECT_SLUG
from tests.pilotis.test_tools import (
    ENV_AWS_ACCESS_KEY_ID,
    ENV_AWS_DEFAULT_REGION,
    ENV_AWS_SECRET_ACCESS_KEY,
    fake_pilotis_initialization,
    set_aws_environment_variables,
)

MISSING_ENV_VARIABLE_ERROR_MESSAGE = "Missing AWS environment variable"
NOT_A_PILOTIS_DIRECTORY_ERROR_MESSAGE = (
    "This command should be run in a pilotis project directory"
)

COMMAND_SUCCESS_CODE = 0

AWS_INQUIRING_DATA = {
    OPTION_AWS_DATA_BUCKET_NAME: DATA_BUCKET_NAME,
    OPTION_AWS_TERRAFORM_BUCKET_NAME: TF_BUCKET_NAME,
    OPTION_AWS_TERRAFORM_BUCKET_DIRECTORY: PROJECT_PATH_ON_BUCKET,
}


@pytest.fixture(name="cli_runner")
def runner():
    return CliRunner()


def test_pilotis_help_should_be_available(cli_runner):
    result = cli_runner.invoke(main, ["--help"])
    assert_that(result.exit_code, equal_to(COMMAND_SUCCESS_CODE), "Exit code")


def test_pilotis_help_display_init_command_help(cli_runner):
    result = cli_runner.invoke(main, ["--help"])
    assert_that(
        result.output,
        matches_regexp("init *Initialize project: generate base folders & files."),
    )


def test_pilotis_help_display_git_command_help(cli_runner):
    result = cli_runner.invoke(main, ["--help"])
    assert_that(result.output, matches_regexp("git *Version with Git & create remote."))


def test_pilotis_help_display_aws_command_help(cli_runner):
    result = cli_runner.invoke(main, ["--help"])
    assert_that(result.output, matches_regexp("aws *Use AWS as a technical backend"))


def test_pilotis_aws_help_should_display_aws_command_long_help(cli_runner):
    result = cli_runner.invoke(main, ["aws", "--help"])
    assert_that(result.output, contains_string("Use AWS as a technical backend"))
    assert_that(
        result.output,
        matches_regexp("--project-dir *DIRECTORY *Directory containing the project"),
    )
    assert_that(
        result.output,
        matches_regexp("--skip-install *Skip post installation scripts"),
    )


def test_pilotis_help_display_main_usage(cli_runner):
    result = cli_runner.invoke(main, ["--help"])
    assert_that(
        result.output,
        contains_string("Usage: pilotis [OPTIONS] COMMAND [ARGS]..."),
    )


def test_pilotis_git_should_fail_if_not_in_an_pilotis_project_directory(tmp_path):
    # When
    result = CliRunner().invoke(
        main,
        ["git", f"--project-dir={str(tmp_path)}"],
        catch_exceptions=False,
        input="\n",
    )

    # Then
    assert_that(result.exit_code, not_(equal_to(COMMAND_SUCCESS_CODE)))
    assert_that(result.stdout, contains_string(NOT_A_PILOTIS_DIRECTORY_ERROR_MESSAGE))


def test_pilotis_aws_should_fail_if_missing_aws_secret_key(tmp_path):
    # Given
    set_aws_environment_variables()
    os.environ.pop(ENV_AWS_SECRET_ACCESS_KEY)
    fake_pilotis_initialization(tmp_path)

    # When
    result = CliRunner().invoke(
        main,
        ["aws", f"--project-dir={str(tmp_path)}", "--skip-install"],
        catch_exceptions=False,
    )

    # Then
    assert_that(result.exit_code, not_(equal_to(COMMAND_SUCCESS_CODE)))
    env_vars = {
        ENV_AWS_ACCESS_KEY_ID,
        ENV_AWS_DEFAULT_REGION,
        ENV_AWS_SECRET_ACCESS_KEY,
    }
    _contains_all_strings(
        result.stdout, {MISSING_ENV_VARIABLE_ERROR_MESSAGE}.union(env_vars)
    )


def test_pilotis_aws_should_fail_if_missing_aws_region(tmp_path):
    # Given
    set_aws_environment_variables()
    os.environ.pop(ENV_AWS_DEFAULT_REGION)
    fake_pilotis_initialization(tmp_path)

    # When
    result = CliRunner().invoke(
        main,
        ["aws", f"--project-dir={str(tmp_path)}", "--skip-install"],
        catch_exceptions=False,
    )

    # Then
    assert_that(result.exit_code, not_(equal_to(COMMAND_SUCCESS_CODE)))
    env_vars = {
        ENV_AWS_ACCESS_KEY_ID,
        ENV_AWS_DEFAULT_REGION,
        ENV_AWS_SECRET_ACCESS_KEY,
    }
    _contains_all_strings(
        result.stdout, {MISSING_ENV_VARIABLE_ERROR_MESSAGE}.union(env_vars)
    )


def test_pilotis_aws_should_fail_if_not_in_an_pilotis_project_directory(tmp_path):
    # Given
    set_aws_environment_variables()

    # When
    result = CliRunner().invoke(
        main,
        ["aws", f"--project-dir={str(tmp_path)}", "--skip-install"],
        catch_exceptions=False,
    )

    # Then
    assert_that(result.exit_code, not_(equal_to(COMMAND_SUCCESS_CODE)))
    assert_that(result.stdout, contains_string(NOT_A_PILOTIS_DIRECTORY_ERROR_MESSAGE))


def test_pilotis_aws_should_inform_the_user_about_project_name(
    tmp_path, mocker, cli_runner
):
    # Given
    set_aws_environment_variables()
    mocker.patch("pilotis.commands.aws.call_shell")
    project_slug = "project-slug"

    # When
    result = cli_runner.invoke(
        main,
        [
            "aws",
            f"--project-dir={str(tmp_path / project_slug)}",
            "--skip-install",
        ],
        catch_exceptions=False,
    )

    # Then
    assert_that(result.output, contains_string(f"Generating AWS on {project_slug}"))


def test_pilotis_aws_should_inform_the_user_about_project_name_given_dot_as_project_dir(
    tmp_path, mocker, cli_runner, monkeypatch
):
    # Given
    set_aws_environment_variables()
    mocker.patch("pilotis.commands.aws.call_shell")
    project_slug = "test-project"
    project_path = tmp_path / project_slug
    project_path.mkdir()
    monkeypatch.chdir(project_path)

    # When
    result = cli_runner.invoke(
        main,
        ["aws", "--project-dir=.", "--skip-install"],
        catch_exceptions=False,
    )

    # Then
    assert_that(result.output, contains_string(f"Generating AWS on {project_slug}"))


def test_pilotis_aws_should_fail_if_missing_aws_access_key(tmp_path, cli_runner):
    # Given
    set_aws_environment_variables()
    os.environ.pop(ENV_AWS_ACCESS_KEY_ID)
    fake_pilotis_initialization(tmp_path)

    # When
    result = cli_runner.invoke(
        main,
        ["aws", f"--project-dir={str(tmp_path)}", "--skip-install"],
        catch_exceptions=False,
    )

    # Then
    assert_that(result.exit_code, not_(equal_to(COMMAND_SUCCESS_CODE)))
    env_vars = {
        ENV_AWS_ACCESS_KEY_ID,
        ENV_AWS_DEFAULT_REGION,
        ENV_AWS_SECRET_ACCESS_KEY,
    }
    _contains_all_strings(
        result.stdout, {MISSING_ENV_VARIABLE_ERROR_MESSAGE}.union(env_vars)
    )


def _contains_all_strings(string_to_check: str, search_strings: Set[str]) -> None:
    for search_string in search_strings:
        assert_that(string_to_check, contains_string(search_string))


def test_pilotis_aws_should_parse_parameters(
    tmp_path, cli_runner, mocker: MockerFixture
):
    # Given
    mocker.patch(
        "pilotis.commands.aws._inquire_user_inputs",
        new=lambda _: AWS_INQUIRING_DATA,
    )
    generate_aws_function_stub = mocker.patch("pilotis.commands.aws.generate_aws")
    fake_pilotis_initialization(tmp_path)
    set_aws_environment_variables()

    # When
    result = cli_runner.invoke(
        main, ["aws", f"--project-dir={str(tmp_path)}", "--skip-install"]
    )

    # Then
    assert_that(result.exit_code, equal_to(COMMAND_SUCCESS_CODE))
    expected_substitution_data = {
        OPTION_PROJECT_SLUG: slugify(tmp_path.name),
        **AWS_INQUIRING_DATA,
    }
    generate_aws_function_stub.assert_has_calls(
        [
            call(tmp_path, expected_substitution_data, True),
        ]
    )


def test_pilotis_aws_should_default_skip_install_to_false(
    tmp_path, cli_runner, mocker: MockerFixture
):
    # Given
    mocker.patch(
        "pilotis.commands.aws._inquire_user_inputs",
        new=lambda _: AWS_INQUIRING_DATA,
    )
    generate_aws_function_stub = mocker.patch("pilotis.commands.aws.generate_aws")
    fake_pilotis_initialization(tmp_path)
    set_aws_environment_variables()

    # When
    result = cli_runner.invoke(main, ["aws", f"--project-dir={str(tmp_path)}"])

    # Then
    assert_that(result.exit_code, equal_to(COMMAND_SUCCESS_CODE))
    expected_substitution_data = {
        OPTION_PROJECT_SLUG: slugify(tmp_path.name),
        **AWS_INQUIRING_DATA,
    }
    generate_aws_function_stub.assert_has_calls(
        [
            call(tmp_path, expected_substitution_data, False),
        ]
    )


def test_pilotis_aws_should_default_project_dir_to_current_dir(
    cli_runner, mocker: MockerFixture
):
    # Given
    mocker.patch(
        "pilotis.commands.aws._inquire_user_inputs",
        new=lambda _: AWS_INQUIRING_DATA,
    )
    generate_aws_function_stub = mocker.patch("pilotis.commands.aws.generate_aws")
    set_aws_environment_variables()

    with cli_runner.isolated_filesystem() as dir_name:
        dir_path = Path(dir_name).resolve()
        fake_pilotis_initialization(dir_path)

        # When
        result = cli_runner.invoke(main, ["aws", "--skip-install"])

        # Then
        assert_that(result.exit_code, equal_to(COMMAND_SUCCESS_CODE))
        expected_substitution_data = {
            OPTION_PROJECT_SLUG: slugify(dir_path.name),
            **AWS_INQUIRING_DATA,
        }
        generate_aws_function_stub.assert_has_calls(
            [
                call(dir_path, expected_substitution_data, True),
            ]
        )


def test_init_should_call_not_call_submodule_if_not_asked_when_inquired(
    cli_runner, mocker: MockerFixture
):
    # Given
    mocker.patch(
        "pilotis.commands.init._inquire_user_inputs",
        new=lambda: {
            OPTION_PROJECT_NAME: GITLAB_DATA[OPTION_PROJECT_SLUG],
            OPTION_TOOLS_TO_SETUP: [],
        },
    )

    generate_aws_function_stub = mocker.patch("pilotis.commands.aws.generate_aws")
    generate_git_function_stub = mocker.patch("pilotis.commands.git.generate")

    with cli_runner.isolated_filesystem() as dir_name:
        dir_path = Path(dir_name).resolve()

        # When
        result = cli_runner.invoke(
            main,
            ["init", "--skip-install", f"--project-parent-dir={str(dir_path)}"],
        )

        # Then
        assert_that(result.exit_code, equal_to(COMMAND_SUCCESS_CODE))
        generate_aws_function_stub.assert_not_called()
        generate_git_function_stub.assert_not_called()


def test_init_should_call_git_module_if_asked_when_inquired(
    cli_runner, mocker: MockerFixture
):
    # Given
    mocker.patch(
        "pilotis.commands.init._inquire_user_inputs",
        new=lambda: {
            OPTION_PROJECT_NAME: TEST_PROJECT_SLUG,
            OPTION_TOOLS_TO_SETUP: [Tools.GIT.value],
        },
    )

    mocker.patch("pilotis.commands.git._inquire_user_inputs", new=lambda: GITLAB_DATA)
    generate_git_function_stub = mocker.patch("pilotis.commands.git.generate")

    with cli_runner.isolated_filesystem() as dir_name:
        dir_path = Path(dir_name).resolve()

        # When
        result = cli_runner.invoke(
            main,
            ["init", "--skip-install", f"--project-parent-dir={str(dir_path)}"],
        )

        # Then
        assert_that(result.exit_code, equal_to(COMMAND_SUCCESS_CODE))
        generate_git_function_stub.assert_has_calls(
            [
                call(dir_path / TEST_PROJECT_SLUG, GITLAB_DATA, True),
            ]
        )


def test_init_should_call_aws_module_if_asked_when_inquired(
    cli_runner, mocker: MockerFixture
):
    # Given
    mocker.patch(
        "pilotis.commands.init._inquire_user_inputs",
        new=lambda: {
            OPTION_PROJECT_NAME: AWS_DATA[OPTION_PROJECT_SLUG],
            OPTION_TOOLS_TO_SETUP: [Tools.AWS.value],
        },
    )

    mocker.patch("pilotis.commands.aws._inquire_user_inputs", new=lambda _: AWS_DATA)
    generate_aws_function_stub = mocker.patch("pilotis.commands.aws.generate_aws")

    with cli_runner.isolated_filesystem() as dir_name:
        dir_path = Path(dir_name).resolve()

        # When
        result = cli_runner.invoke(
            main,
            ["init", "--skip-install", f"--project-parent-dir={str(dir_path)}"],
        )

        # Then
        assert_that(result.exit_code, equal_to(COMMAND_SUCCESS_CODE))
        generate_aws_function_stub.assert_has_calls(
            [
                call(dir_path / AWS_DATA[OPTION_PROJECT_SLUG], AWS_DATA, True),
            ]
        )
