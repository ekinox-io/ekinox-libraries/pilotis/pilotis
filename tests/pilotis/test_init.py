import os
from pathlib import Path
from subprocess import call

import yaml
from hamcrest import assert_that, is_, only_contains

from pilotis.commands import (
    OPTION_PROJECT_NAME,
    OPTION_PROJECT_SLUG,
    OPTION_PYTHON_PACKAGE_NAME,
)
from pilotis.commands.aws import AWS_DATA_STORAGE_DOC_SECTION
from pilotis.commands.init import generate
from pilotis.domain.python_project_config import PYTHON_PROJECT_DIRECTORY_NAME
from tests.pilotis.test_aws import TEST_PROJECT_PYTHON_PACKAGE_NAME

TEST_PROJECT_NAME = "pilotis test project"
TEST_PROJECT_SLUG = "pilotis-test-project"
TEST_PYTHON_PACKAGE_NAME = "pilotis_test_project"
GREP_NO_MATCHING_RETURN_CODE = 1


def test_init_should_add_project_files_with_replaced_templates(tmp_path):
    # Given
    init_data = {
        OPTION_PROJECT_NAME: TEST_PROJECT_NAME,
        OPTION_PROJECT_SLUG: TEST_PROJECT_SLUG,
        OPTION_PYTHON_PACKAGE_NAME: TEST_PYTHON_PACKAGE_NAME,
    }

    # When
    generate(tmp_path, substitution_data=init_data, skip_install=True)

    # Then
    project_dir = Path(tmp_path) / TEST_PROJECT_SLUG
    assert_folder_structure_exists(project_dir)
    assert_that(
        (project_dir / "Makefile").exists(),
        "init should add initialize Makefile",
    )

    for script_name in ["check_tools.bash", "pyenv_config.bash"]:
        file = project_dir / "scripts" / script_name
        assert_that(os.access(file, os.X_OK), is_(True), f"{file} should be executable")

    assert_jinja_tags_are_replaced(project_dir)


def test_init_should_make_build_script_executable(tmp_path):
    # Given
    init_data = {
        OPTION_PROJECT_NAME: TEST_PROJECT_NAME,
        OPTION_PROJECT_SLUG: TEST_PROJECT_SLUG,
        OPTION_PYTHON_PACKAGE_NAME: TEST_PYTHON_PACKAGE_NAME,
    }

    # When
    generate(tmp_path, substitution_data=init_data, skip_install=True)

    # Then
    project_dir = Path(tmp_path) / TEST_PROJECT_SLUG
    container_dir = project_dir / "containers"
    containers = ["base", "dash", "tests"]

    for container_name in containers:
        build_script = container_dir / container_name / "build.sh"
        assert_that(
            os.access(build_script, os.X_OK),
            is_(True),
            f"{build_script} should be executable",
        )


def test_init_should_flag_project_directory_as_pilotis_project_directory(tmp_path):
    # Given
    init_data = {
        OPTION_PROJECT_NAME: TEST_PROJECT_NAME,
        OPTION_PROJECT_SLUG: TEST_PROJECT_SLUG,
        OPTION_PYTHON_PACKAGE_NAME: TEST_PYTHON_PACKAGE_NAME,
    }

    # When
    generate(tmp_path, substitution_data=init_data, skip_install=True)

    # Then
    pilotis_config_file_path = Path(tmp_path) / TEST_PROJECT_SLUG / "pilotis.config"
    assert_that(pilotis_config_file_path.exists())


def test_generate_aws_should_add_aws_doc(tmp_path: Path):
    # Given
    init_data = {
        OPTION_PROJECT_NAME: TEST_PROJECT_NAME,
        OPTION_PROJECT_SLUG: TEST_PROJECT_SLUG,
        OPTION_PYTHON_PACKAGE_NAME: TEST_PROJECT_PYTHON_PACKAGE_NAME,
    }
    project_path: Path = tmp_path / TEST_PROJECT_SLUG
    python_project_path = project_path / PYTHON_PROJECT_DIRECTORY_NAME
    mkdocs_config_file = python_project_path / "mkdocs.yml"
    aws_docs_page = python_project_path / "docs" / "aws-data-storage.md"

    # When
    generate(tmp_path, substitution_data=init_data, skip_install=True)

    # Then
    assert_that(mkdocs_config_file.exists())
    with open(mkdocs_config_file, "r", encoding="utf-8") as readme_file_stream:
        mkdocs_config_content = yaml.load(readme_file_stream, Loader=yaml.FullLoader)
    assert_that(AWS_DATA_STORAGE_DOC_SECTION not in mkdocs_config_content["nav"])
    assert_that(not aws_docs_page.exists())


def assert_folder_structure_exists(project_dir):
    created_elements_in_project_dir = [
        x.name for x in project_dir.iterdir() if x.is_dir()
    ]
    # noinspection PyTypeChecker
    assert_that(
        created_elements_in_project_dir,
        only_contains("python", "workdir", "containers", "scripts", "makefiles"),
    )


def assert_jinja_tags_are_replaced(project_dir):
    assert_that(
        call(f"grep -rIl {OPTION_PROJECT_NAME} {project_dir}", shell=True),
        is_(GREP_NO_MATCHING_RETURN_CODE),
        "Jinja project name tags should be replaced with given value",
    )
    assert_that(
        call(f"grep -rIl {OPTION_PROJECT_SLUG} {project_dir}", shell=True),
        is_(GREP_NO_MATCHING_RETURN_CODE),
        "Jinja project slug tags should be replaced with given value",
    )
