# Project versioning

| Git provider        | Project initialization     | CI/CD  |
| ------------- |:-------------:| -----:|
| Gitlab      | ✅ | ✅ |
| GitHub      | ✅  | ❌ |

In order to start versioning your project, just run `pilotis git` in a terminal, inside your project. If you want to create it in another directory, you can also run `pilotis git --project-dir my/project/directory/path`.

Pilotis will ask you to choose your git provider (`gitlab` or `github`), and some other basic information to be able to publish your project on your account.

The command will then transform your project into a git project, and add a `.gitlab-ci.yml` file if you chose `gitlab` as a provider.
