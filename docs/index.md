# Introduction

Pilotis is an open source ML project generator aiming to quickly bootstrap projects with all the necessary tools and conventions. It allows you to focus on what is really creating value for you, while helping you to create and maintain a project with high quality standards (automatic tests, CI/CD, architecture...).

To install Pilotis, just run `pip install pilotis` in your terminal, inside or outside a virtual environment according to what suits you best.
