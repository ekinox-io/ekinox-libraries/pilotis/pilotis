#!/bin/bash -eu

SCRIPT_DIR=$(dirname $0)

cd /app
${SCRIPT_DIR}/generate-project.bash

cd my-cool-project/python
make test

cd .. && make generate-example && cd python
poetry run python -m my_cool_project.application.parse_example --workdir ../workdir --dataset-name dataset_example --dataset-version version_1
poetry run python -m notebooks.pilotis_onboarding

echo "Everything is working fine"
fix