#!/usr/bin/expect -f

set timeout -1
set send_slow {1 .005}
spawn pilotis init

expect "*Project name?: *"
send -s "My cool project\r"

expect "*Which tool do you want Pilotis to setup in your project?*"
send -s "\r"

expect EOF
