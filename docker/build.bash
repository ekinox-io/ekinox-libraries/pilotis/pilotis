#!/bin/bash

set -eu

# Ugly but more universal than readlink...
BASEDIR_ABSOLUTE_PATH=$(cd "$(dirname "$0")" && pwd)

docker build -t "ekinoxio/pilotis:end_to_end" -f "$BASEDIR_ABSOLUTE_PATH/Dockerfile" "$BASEDIR_ABSOLUTE_PATH/.."

echo "Run the following command to run the end-to-end test"
echo "$ docker run --rm -it -v $BASEDIR_ABSOLUTE_PATH/:/scripts/ ekinoxio/pilotis:end_to_end /scripts/test-end-to-end.bash"

echo "Run this one in order to be able to run manual test from inside the container"
echo "$ docker run --rm -it -p 127.0.0.1:8000:8000 ekinoxio/pilotis:end_to_end /bin/bash"
