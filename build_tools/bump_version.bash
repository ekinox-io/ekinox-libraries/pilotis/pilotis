#!/usr/bin/env bash

set -eu

function gitlab_find_project_pipeline_jobs {
    curl --silent --show-error --location \
        --request GET \
        --header "PRIVATE-TOKEN: ${RUNNER_PUSH_TOKEN}" \
        --url "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/pipelines/${CI_PIPELINE_ID}/jobs"
}

function gitlab_find_started_bump_version_jobs_count {
  echo $(gitlab_find_project_pipeline_jobs) | \
      jq '[ .[] | select(.started_at != null and .stage == "bump-version") ] | length'
}

function bump_version {
    git remote add gitlab_origin \
      "https://oauth2:${RUNNER_PUSH_TOKEN}@gitlab.com/ekinox-io/ekinox-libraries/pilotis/pilotis-test-ci.git"
    git fetch gitlab_origin
    git checkout -b master gitlab_origin/master

    SHORT_SHA=$(git rev-parse --short=8 HEAD)
    if [[ $SHORT_SHA != $CI_COMMIT_SHORT_SHA ]]
    then
        echo "Current HEAD (${SHORT_SHA}) is not the commit beeing built ($CI_COMMIT_SHORT_SHA)"
        echo "Stopping the bump version operation"
        exit 1
    fi

    poetry version $BUMP_TYPE

    new_version="$(poetry version -s)"
    git commit -a -m "Bump version to ${new_version}"
    git push gitlab_origin master --push-option="ci.skip"
    git tag "${new_version}"
    git push gitlab_origin "${new_version}"
}


function main {
  echo "Bumping version ${BUMP_TYPE} of master"

  started_bump_version_count=$(gitlab_find_started_bump_version_jobs_count)
  if [[ $started_bump_version_count > 1 ]]
  then
      echo "There already is a job running to bump the version"
      exit 1
  fi

  bump_version
}

main $@
