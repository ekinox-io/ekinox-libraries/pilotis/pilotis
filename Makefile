# file modified from https://gist.github.com/lumengxi/0ae4645124cd4066f676
.PHONY: *

#################################################################
# Shared variables
#################################################################

PACKAGE_DIR=pilotis_io

define BROWSER_PYSCRIPT
import os, webbrowser, sys
try:
	from urllib import pathname2url
except:
	from urllib.request import pathname2url

webbrowser.open("file://" + pathname2url(os.path.abspath(sys.argv[1])))
endef
export BROWSER_PYSCRIPT
BROWSER := python -c "$$BROWSER_PYSCRIPT"

# used to expose poetry
export PATH := /root/.local/bin:${PATH}

#################################################################
# Shared functions
#################################################################

# Check that given variables are set and all have non-empty values,
# die with an error otherwise.
#
# Params:
#   1. Variable name(s) to test.
#   2. (optional) Error message to print.
# Details at https://stackoverflow.com/a/10858332/4374048
check_defined = \
    $(strip $(foreach 1,$1, \
        $(call __check_defined,$1,$(strip $(value 2)))))
__check_defined = \
    $(if $(value $1),, \
      $(error Undefined $1$(if $2, ($2)): please pass as argument (see help target)))

#################################################################
# setting dependencies for automation (tox, cicd)
#################################################################

install-test-dependencies :
	poetry install --only main --no-root -E test

#################################################################
# setting up ci-cd env
#################################################################

setup-cicd-python3:
	update-alternatives --install /usr/bin/python python /usr/bin/python3 1
	curl -sSL  https://bootstrap.pypa.io/get-pip.py | python

setup-poetry:
	curl -sSL https://install.python-poetry.org | python3 -
	poetry config virtualenvs.create false
ifneq ("$(wildcard $(PATH_TO_FILE))","")
	poetry env use $(cat .python-version)
endif

setup-cicd-git:
	git config --global user.email "cd-pipeline@ekinox.io"
	git config --global user.name "CICD Pipeline"

setup-cicd-test-stage: setup-poetry setup-cicd-git

####################
## Quality checks ##
####################

unit-test:
	poetry install -E test --no-root
	poetry run pytest

format-check-black:
	poetry install -E quality -E test --no-root
	poetry run black --check pilotis tests

format-check-isort:
	poetry install -E quality -E test --no-root
	poetry run isort -c pilotis tests

format-check: format-check-black format-check-isort

format-black:
	poetry install -E quality -E test --no-root
	poetry run black pilotis tests

format-isort:
	poetry install -E quality -E test --no-root
	poetry run isort pilotis tests

format: format-black format-isort

lint-flake8:
	poetry install -E quality -E test --no-root
	poetry run flake8 pilotis tests

lint-pylint:
	poetry install -E quality -E test --no-root
	poetry run pylint pilotis tests

lint: lint-flake8 lint-pylint

types-mypy:
	poetry install -E quality -E test --no-root
	poetry run mypy pilotis

types: types-mypy

quality-checks: unit-test format-check lint types

bump-version:
	build_tools/bump_version.bash

